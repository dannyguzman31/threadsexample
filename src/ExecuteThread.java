import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThread {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("**********************************************");
        System.out.println("        Welcome to Skynet Video System");
        System.out.print("Enter number of cameras:");
        int numbOfCameras = input.nextInt();
        if (numbOfCameras == 1){
            System.out.print("Enter Camera name: ");
            String cName = input.next();
            ThreadsExample ct1 = new ThreadsExample(cName);
            ct1.start();
        }
        else if (numbOfCameras == 2){

            System.out.print("Enter Camera name: ");
            String cName = input.next();
            System.out.print("Enter Camera name: ");
            String c2Name = input.next();
            ThreadsExample ct = new ThreadsExample(cName);
            ct.start();
            ThreadsExample ct2 = new ThreadsExample(c2Name);
            ct2.start();
        }
        else if (numbOfCameras == 3){

            System.out.print("Enter Camera name: ");
            String cName = input.next();
            System.out.print("Enter Camera name: ");
            String c2Name = input.next();
            System.out.print("Enter Camera name: ");
            String c3Name = input.next();
            ThreadsExample ct = new ThreadsExample(cName);
            ct.start();
            ThreadsExample ct2 = new ThreadsExample(c2Name);
            ct2.start();
            ThreadsExample ct3 = new ThreadsExample(c3Name);
            ct3.start();
        }
        else {
            System.out.println("I can only do 3 cameras at the time");
        }
    }
}
