import java.util.Random;

public class ThreadsExample implements Runnable {

    private Thread t;
    final String cameraName;
    int time;
    Random r = new Random();


    ThreadsExample(String name ){
        cameraName = name;
        time = r.nextInt(999);
        System.out.println(cameraName + " has been detected...");
    }

        public void run(){
            try{
                System.out.println("Searching for Network...");
                for (int i = 0; i < 10; i ++){
                    System.out.print(".");
                    Thread.sleep(time);
                }
                System.out.println();
                System.out.print("Networks found!");
                Thread.sleep(time);
                System.out.println();
                System.out.print("Connecting " + cameraName + " to Network...");
                for ( int i = 0; i < 15; i ++){
                    System.out.print(".");
                    Thread.sleep(time);
                }
            }catch (Exception e){
                System.out.println("Connection Failed! Try again...");
            }

            System.out.println();
            System.out.println(cameraName + " Successfully connected");
            System.out.println(cameraName + " Camera is ready!");
        }

    public void start() {

        if (t == null ){
            t = new Thread(this, cameraName);
            t.start();
        }
    }
}
